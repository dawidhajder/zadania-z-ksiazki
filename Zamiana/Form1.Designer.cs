﻿namespace Zamiana
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lloyd_button = new System.Windows.Forms.Button();
            this.Lucinada_button = new System.Windows.Forms.Button();
            this.Change = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Lloyd_button
            // 
            this.Lloyd_button.Location = new System.Drawing.Point(45, 48);
            this.Lloyd_button.Name = "Lloyd_button";
            this.Lloyd_button.Size = new System.Drawing.Size(162, 61);
            this.Lloyd_button.TabIndex = 0;
            this.Lloyd_button.Text = "Lloyd";
            this.Lloyd_button.UseVisualStyleBackColor = true;
            this.Lloyd_button.Click += new System.EventHandler(this.Lloyd_button_Click);
            // 
            // Lucinada_button
            // 
            this.Lucinada_button.Location = new System.Drawing.Point(45, 160);
            this.Lucinada_button.Name = "Lucinada_button";
            this.Lucinada_button.Size = new System.Drawing.Size(162, 68);
            this.Lucinada_button.TabIndex = 1;
            this.Lucinada_button.Text = "Lucinada";
            this.Lucinada_button.UseVisualStyleBackColor = true;
            this.Lucinada_button.Click += new System.EventHandler(this.Lucinada_button_Click);
            // 
            // Change
            // 
            this.Change.Location = new System.Drawing.Point(45, 281);
            this.Change.Name = "Change";
            this.Change.Size = new System.Drawing.Size(162, 68);
            this.Change.TabIndex = 2;
            this.Change.Text = "Zamień!";
            this.Change.UseVisualStyleBackColor = true;
            this.Change.Click += new System.EventHandler(this.Change_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(45, 367);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(162, 51);
            this.button1.TabIndex = 3;
            this.button1.Text = "2 referencje";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 430);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Change);
            this.Controls.Add(this.Lucinada_button);
            this.Controls.Add(this.Lloyd_button);
            this.Name = "Form1";
            this.Text = "Zamiana";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Lloyd_button;
        private System.Windows.Forms.Button Lucinada_button;
        private System.Windows.Forms.Button Change;
        private System.Windows.Forms.Button button1;
    }
}

