﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zamiana
{
    public partial class Form1 : Form
    {
        Elephant lloyd;
        Elephant lucinada;
        Elephant forChange;

        //Elephant 
        //Elephant 

        public Form1()
        {
            InitializeComponent();
            lucinada = new Elephant() { Name = "Lucinada", EarSize = 33 };
            lloyd = new Elephant() { Name = "Lloyd", EarSize = 40 };
        }


        private void Lucinada_button_Click(object sender, EventArgs e)
        {
            lucinada.WhoAmI();
        }
        private void Lloyd_button_Click(object sender, EventArgs e)
        {
            lloyd.WhoAmI();
        }

        private void Change_Click(object sender, EventArgs e)
        {
            forChange = lloyd;
            lloyd = lucinada;
            lucinada = forChange;
            MessageBox.Show("Obiekty zamienione.");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            lloyd.TellMe("Cześć", lucinada);

            lloyd = lucinada;
            lloyd.EarSize = 4321;
            lloyd.WhoAmI();
        }
    }
}
