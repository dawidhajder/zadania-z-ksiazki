﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odbijające_się_etykiety
{
    public partial class Form1 : Form // Dlaczego po dwukropku jest Form?
    {
        public Form1() //Deklaracja klasy dla zainicjalizowania komponentu?
        {
            InitializeComponent();
        }
        LabelBouncer[] bouncers = new LabelBouncer[3]; // Utworzenie tablicy obiektów bouncers[0,1,2]
        private void ToggleBouncing(int index, Label labelToBounce) //
        {
            if (bouncers[index] == null) //jeśli null to działamy, na początku jest null
            {
                bouncers[index] = new LabelBouncer();//bouncers[0] staje się naklejką do obiektu
                bouncers[index].MyLabel = labelToBounce;//Do pola MyLabel przypisuję prostokącik label1
            }
            else
            {
                bouncers[index] = null;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            ToggleBouncing(0, label1); //Wysyłam 0 i label1 (ten ten prostokącik label1 raczej) do metody
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            ToggleBouncing(1, label2);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            ToggleBouncing(2, label3);
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                if (bouncers[i] != null)
                {
                    bouncers[i].Move();
                }
            }
        }
    }
}
