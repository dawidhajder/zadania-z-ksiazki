﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odbijające_się_etykiety
{
    class LabelBouncer
    {
        public Label MyLabel; //Będzie przechowywać nalepkę do przedmiotu Label. Początkowo null.
                    //W tym polu zapiszę referencję do jednej z 3 etykiet (Label), które będą się poruszać.
        public bool GoingForward = true; //Raz true, raz false, przy odbijaniu.

        public void Move()
        {
            if (MyLabel != null)
            {
                if (GoingForward == true)
                {
                    MyLabel.Left += 5;
                    if (MyLabel.Left >= MyLabel.Parent.Width - MyLabel.Width)
                    {
                        GoingForward = false;
                    }
                }
                else
                {
                    MyLabel.Left -= 5;
                    if (MyLabel.Left <= 0)
                    {
                        GoingForward = true;
                    }

                }
            }
        }
    }
}
