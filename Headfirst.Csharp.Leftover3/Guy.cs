﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Headfirst.Csharp.Leftover3
{
    public class Guy
    {
        public string name;
        public int number1;
        public int number2;

        //static void Main(string[] args)
        //{
        //    Guy guy = new Guy("Janek", 43, 125);
           // HiThereWriter.HiThere(guy.Name);
       // }
       public Guy(string name, int number1, int number2)
        {
            this.name = name;
            this.number1 = number1;
            this.number2 = number2;
        }
        
    }
}
