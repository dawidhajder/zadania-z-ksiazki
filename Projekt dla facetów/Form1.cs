﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_dla_facetów
{
    public partial class Form1 : Form
    {
        Guy joe;
        Guy bob ;
        int bank = 100;
        public void UpdateForm()
        {
            joesCashLabel.Text = joe.Name + " ma " + joe.Cash + " zł";
            bobsCashLabel.Text = bob.Name + " ma " + bob.Cash + " zł";
            bankCashLabel.Text = "Bank ma " + bank + " zł";
               
        }
       
        public Form1()
        {
            InitializeComponent();

            //bob = new Guy();
            //bob.Cash = 100;
            //bob.Name = "Bob";
            bob = new Guy() { Cash = 100, Name = "Bob" };

            //joe = new Guy();
            //joe.Cash = 50;
            //joe.Name = "Joe";
            joe = new Guy() { Cash = 50, Name = "Joe" };

            UpdateForm();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (bank >= 10)
            {
                bank -= joe.ReceiveCash(10);
                UpdateForm();
            }
            else
                MessageBox.Show("Bank nie posiada takiej ilości pieniędzy");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }

        private void JoeGivesToBob_Click(object sender, EventArgs e)
        {
            if (joe.Cash >= 10)
            {
                joe.Cash -= bob.ReceiveCash(10);
                UpdateForm();
            }
            else
                MessageBox.Show("Joe nie posiada takiej ilości pieniędzy.");
        }

        private void BobGivesToJoe_Click(object sender, EventArgs e)
        {
            if (bob.Cash >= 5)
            {
                bob.Cash -= joe.ReceiveCash(5);
                UpdateForm();
            }
            else
                MessageBox.Show("Bob nie posiada takiej ilości pieniędzy.");
        }
    }
}
