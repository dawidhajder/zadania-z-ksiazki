﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zbudujmy_dom_2
{
    interface IHasExteriorDoor
    {
        string DoorDescription { get; }
        //string DoorLocation { get; set; } // Czemu set?
        Location DoorLocation { get; set; } // :O
    }
}
