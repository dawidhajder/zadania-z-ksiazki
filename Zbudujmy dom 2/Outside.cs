﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zbudujmy_dom_2
{
    class Outside : Location
    {
        private bool hot;


        public Outside (string name, bool hot) 
            : base(name)
        {
            this.hot = hot;
        }
      public override string Description
        {
            get
            {
                // BYŁO string newDescription = base.Description;
                string newDescription = base.Description;
                if (hot)
                    // BYŁO NewDescription += " Tutaj jest bardzo gorąco.";
                    newDescription += " Tutaj jest bardzo gorąco.";
                // było return NewDescription;
                return newDescription;
            }
        }
    }
}
