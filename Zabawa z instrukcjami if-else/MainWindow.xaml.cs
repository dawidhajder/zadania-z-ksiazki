﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Zabawa_z_instrukcjami_if_else
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        int jednorazowe = 1;
        private void ChangeText_Click(object sender, RoutedEventArgs e)
        {
           
                if (enableCheckbox.IsChecked == true)
                {


                   if( jednorazowe == 1 )
                    {
                        labelToChange.HorizontalAlignment = HorizontalAlignment.Left;
                        labelToChange.Text = "Z lewej";
                        jednorazowe--;
                    }


                    if (labelToChange.HorizontalAlignment == HorizontalAlignment.Left)
                    {
                        labelToChange.HorizontalAlignment = HorizontalAlignment.Right;
                        labelToChange.Text = "Z prawej";
                    }
                    else
                    {
                        labelToChange.HorizontalAlignment = HorizontalAlignment.Left;
                        labelToChange.Text = "Z lewej";
                    }

                }
                else
                {
                    labelToChange.Text = "Możliwość zmiany tekstu została wyłączona";
                    labelToChange.HorizontalAlignment = HorizontalAlignment.Center;
                }

        }
    }
}
