﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planista_przyjęć
{
    class DinnerParty
    {
        public int NumberOfPeople;
        decimal CostOfBeveragesPerPerson; // = 25.0M; //napoje
        decimal CostOfDecorations = 0; // 7.5M; //dekoracje
        public const int CostOfFoodPerPerson = 25; //jedzenie

        public void CalculateCostOfDecorations(bool fancy)
        {
            if ( fancy )
            {
                CostOfDecorations = (NumberOfPeople * 15.00M) + 50M; // CostOfDecorations;
            }
            else
            {
                CostOfDecorations = (NumberOfPeople * 7.50M) + 30M;
            }
            
        }
        public void SetHealthyOption(bool healthyOption)
        {
            if (healthyOption)
            {
                CostOfBeveragesPerPerson = 5.00M; //CostOfBeveragesPerPerson = 5 * NumberOfPeople;
            }
            else
            {
                CostOfBeveragesPerPerson = 20.00M;
            }

           
        }
        public decimal CalculateCost(bool healthyOption)
        {
            decimal totalCost = CostOfDecorations + ((CostOfBeveragesPerPerson + CostOfFoodPerPerson) * NumberOfPeople);
            if(healthyOption)
            {
                return totalCost * .95M;
            }
            else
            {
                return totalCost;
            }

        }

    }
}
