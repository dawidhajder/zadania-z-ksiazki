﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dzień_na_wyścigach
{
    public class Greyhound
    {
        public int StartingPosition; // Miejsce, gdzie rozpoczyna się PictureBox
        public int RacetrackingLength; // Jak długa jest trasa
        public PictureBox MyPictureBox = null; //Mój obiekt PictureBox
        public int Location = 0; // Moje połozenie na torze wyścigowym
        public Random MyRandom; // Instancja klasy Random

        public bool Run()
        {
            Location += 1; // Przesuń się do przodu losowo o 1, 2, 3 lub 4 punkty
            // Zaktualizuj połozenie PictureBox na formularzu
            // Zwróc true, jeżeli wygrałem wyścig
        }
        
        public void TakeStartingPosition()
        {
            // Wyzeruj połozenie i ustaw na linii startowej
        }
    }
}
