﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organizator_przyjęć_z_dziedziczeniem
{
    class DinnerParty : Party
    {
       
        public bool HealthyOption { get; set; }

        public DinnerParty(int numberOfPeople, bool healthyOption, bool fancyDecorations)
        {
            NumberOfPeople = numberOfPeople;
            FancyDecorations = fancyDecorations;
            HealthyOption = healthyOption;
        }
    
        private decimal CalculateCostOfBeveragesPerPerson()
        {
            decimal costOfBeveragesPerPerson;
            if (HealthyOption)
            {
                costOfBeveragesPerPerson = 5.00M;
            }
            else
            {
                costOfBeveragesPerPerson = 20.00M;
            }
            return costOfBeveragesPerPerson;
        }
        override public decimal Cost
        {
            get
            {
                decimal totalCost = base.Cost;
                totalCost += CalculateCostOfBeveragesPerPerson() * NumberOfPeople;
                if (HealthyOption)
                    totalCost *= .95M;
                return totalCost;

               // decimal totalCost = CalculateCostOfDecorations();
               // totalCost += ((CalculateCostOfBeveragesPerPerson()
                  //  + CostOfFoodPerPerson) * NumberOfPeople);
                //if (HealthyOption)
                //{
               //     totalCost *= .95M;
               // }
               // if (NumberOfPeople > 12)
               //     totalCost += 100;
              //  return totalCost;
            }
        }
    }
}
