﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zbudujmy_dom
{
    class RoomWithDoor : Room, IHasExteriorDoor
    {
        public RoomWithDoor(string name, string decoration, string doorDescription) : base(name, decoration)
        {
            DoorDescription = doorDescription;
        }

        public string DoorDescription { get; private set; }
        public Location DoorLocation { get; set; }



       // public string DoorDescription
       // {
      //      get { return "testtt"; }
       // }

        //public string DoorLocation
       // {
            //get { return "taaast";  }
        //}

        //string a, b, c;
       // RoomWithDoor livingRoom = new RoomWithDoor();
        //public RoomWithDoor(string a, string b, string c)
        //{
         //   this.a = a;
         //   this.b = b;
         //   this.c = c;
       // }
        // Tutaj znajdzie się właściwość tylko do oczytu DoorLocation
        // Tutaj znajdzie się właściwość DoorDescription
    }
}
