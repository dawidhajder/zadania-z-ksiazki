﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zbudujmy_dom
{
    class OutsideWithDoor : Outside, IHasExteriorDoor
    {
        public OutsideWithDoor(string name, bool hot, string doorDescription) : base(name, hot)
        {
            this.DoorDescription = doorDescription;
        }
        public string DoorDescription
        {
            get; private set;
        }

        public Location DoorLocation { get; set; }

        public override string Description
        {
            get
            {
                return base.Description + " Widzisz teraz " + DoorDescription + ".";
            }
        }
        //public string DoorLocation
        //{
           // get { return "taaast"; }
       // }

       // OutsideWithDoor frontYard = new OutsideWithDoor();

        // Tutaj znajdzie sie właściwość tylko do oczytu DoorLocation
        // Tutaj znajdzie się właściwość DoorDescription
    }
}
