﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zbudujmy_dom
{
    class Room :Location
    {
        private string decoration;

        //Room diningRoom = new Room();

        public Room(string name, string decoration) : base(name)
        {
            this.decoration = decoration;
        }
        public override string Description
        {
            get
            {
                return base.Description + " Widzisz tutaj " + decoration + ".";
            }
        }
    }
}
