﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zbudujmy_dom
{
    class Outside : Location
    {
        private bool hot;
        //Outside garden = new Outside();

        public Outside (string name, bool hot) : base(name)
        {
            this.hot = hot;
        }
      public override string Description
        {
            get
            {
                string NewDescription = base.Description;
                if (hot)
                    NewDescription += " Tutaj jest bardzo gorąco.";
                return NewDescription;
            }
        }
    }
}
