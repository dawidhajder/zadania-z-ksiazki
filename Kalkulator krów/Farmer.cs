﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator_krów
{
    class Farmer
    {
        public Farmer(int numberOfCows, int feeMultiplier)
        {
            this.feedMultiplier = feedMultiplier;
            numberOfCows = numberOfCows;
        }

        public int BagsOfFeed { get; private set; }
        private int feedMultiplier;
        public int FeedMultiplier { get { return feedMultiplier;  } }

        private int numberOfCows;
        public int NumberOfCows
        {
            // (dodaj akcesory get i set z poprzedniej strony)
            get
            {
                return numberOfCows;
            }
            set
            {
                numberOfCows = value;
                BagsOfFeed = numberOfCows * FeedMultiplier;
            }
        }
    }
}
