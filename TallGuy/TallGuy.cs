﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TallGuy
{
    public class TallGuy : IClown
    {
        public string Name;
        public int Height;
        public void TalkAboutYouself()
        {
            Console.WriteLine("Nazywam się " + Name + " i mam "
                + Height + " centymetrów wzrostu.");
        }
        public string FunnyThingIHave
        {
            get { return "duże buty"; }
        }
        public void Honk()
        {
            Console.WriteLine("Tut tuut!");
        }
    }
}
