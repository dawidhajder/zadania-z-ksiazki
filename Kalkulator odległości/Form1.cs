﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator_odległości
{
    public partial class Form1 : Form
    {
        int startingMileage = 0;
        int endingMileage = 0;
        double milesTraveled;
        double reimburseRate = 0.39;
        double amountOwed;

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            startingMileage = (int)numericUpDown1.Value;
            endingMileage = (int)numericUpDown2.Value;
            milesTraveled = endingMileage -= startingMileage;
            if (startingMileage > endingMileage)
            {
                MessageBox.Show("Początkowy stan licznika musi być mniejszy niż końcowy."
                    , "Nie mogę obliczyć odległości");
            }
            
            amountOwed = milesTraveled *= reimburseRate;
            label4.Text = amountOwed.ToString() + " zł";
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            startingMileage = (int)numericUpDown1.Value;
            endingMileage = (int)numericUpDown2.Value;
            milesTraveled = endingMileage -= startingMileage;
            MessageBox.Show(milesTraveled + " kilometrów", "Przebyta odległość");
        }
    }
}
